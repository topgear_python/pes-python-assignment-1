#1	 Python program basic	Write a program to add , Subtract, Multiply, and divide 2 numbers
#--------------------------------------------------------------------------------
#PYTHON CODE:
a = 10
b = 20
print "Addition is ",a+b
print "Subtraction is ",a-b
print "Multiplication is ",a*b
print "Division is ",a/float(b)

#================================================================================
#2	 Basic Operators	Write a program to find the biggest of 3 numbers ( Use If Condition )
#--------------------------------------------------------------------------------
#PYTHON CODE:
a=10
b=20
c=30

if a>b:
    if a>c:
        print a," is greater"
    elif b>c:
        print b," is greater"
    else:
        print c," is greater"
elif b>c:
    print b," is greater"
else:
    print c," is greater"
    
#================================================================================
#3	 Basic operators	Write a program to find  given number is odd or Even
#--------------------------------------------------------------------------------
#PYTHON CODE:
num = int(raw_input("Enter the number to find even or odd!"))

if num%2 == 0:
    print "%s is even"%num
else:
    print "%s is odd"%num
#================================================================================
#4	 Basic operators	Write a program to find the number is Prime or not.
#--------------------------------------------------------------------------------
PYTHON CODE:
num = int(raw_input("Enter the number to find even or odd!"))

if num in [0,1]:
    print "%s is neither"%num
for item in xrange(2,num):
    if num%item == 0:
        print "%s is not a prime"%num
        break
    else:
        print "%s is prime"%num
        break
#================================================================================
#5	 Command line Arguments 	Write a program to receive 5 command line arguments and print each argument separately.
#Example :
#             >> python test.py arg1 arg2 arg3 arg4 arg5
#  a) From the above statement your program should receive arguments and print them each of them. 
#  b) Find the biggest of three numbers, where three numbers are passed as command line arguments.
#--------------------------------------------------------------------------------
#PYTHON CODE:
#a

import sys
for x in sys.argv:
    print "Command line Argument: ", x

#b
import sys
s = []
for x in sys.argv:
    s.append(x)
s.sort(reverse=True)
print "%s is the biggest number"%s[0]
#================================================================================
#6	 Basic String operations	Write a program to read string and print each character separately.
#    a) Slice the string using slice operator [:] slice the portion the strings to create a sub strings.
#    b) Repeat the string 100 times using repeat operator *
#    c) Read strig 2 and  concatenate with other string using + operator.
#--------------------------------------------------------------------------------
#PYTHON CODE:
string = "This is a basic python program"
for item in string:
    print item
#a
str = string[5:15]
print str
#b
print string*100
#c
str2 = "Feeling excited"
string += str2
print string

#================================================================================
#7	 Basic List Operations	Create a list with at least 10 elements in it
#       print all elements
#       perform slicing
#       perform repetition with * operator
#       Perform concatenation wiht other list.
#--------------------------------------------------------------------------------
#PYTHON CODE:
cont = ["1","2","3","4","5","6","7","8","9","0"]

#a
for item in cont:
    print item

#b
print cont[5:8]

#c
print cont*10

#d
cont2 = [10,20,30,40,50,60]
cont += cont2
print cont
#================================================================================
#8	Basic of Tuple	Repeat program 7 with Tuple( Take example from Tutorial )
#--------------------------------------------------------------------------------
#PYTHON CODE:
tpl1 = (1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
# a
for item in tpl1:
    print item
# b
print tpl1[5:8]
# c
print tpl1 * 10
# d
tpl2 = (10, 20, 30, 40)
tpl1 += tpl2
print tpl1

#================================================================================
#9	 Python Numbers	Write program to Add , subtract, multiply, divide 2 complex numbers.
#--------------------------------------------------------------------------------
#PYTHON CODE:
x = complex(1,2)
y = complex(3,4)

print "Addition of x and y is: ", x+y
print "Substraction of x and y is: ", x-y
print "Multiplication of x and y is: ", x*y
print "Division of x and y is: ", x/y
#================================================================================
#10	 Assignment Operator	Using assignment operators, perform following operations
#     Addition, Substation, Multiplication, Division, Modulus, Exponent and Floor division operations
#--------------------------------------------------------------------------------
#PYTHON CODE:
a=b=10
a+=b
print "\nAddition using assignment operations is: %s"%a

a-=b
print "\nSubstraction using assignment operations is: %s"%a

a*=b
print "\nMultiplication using assignment operations is: %s"%a

a/=b
print "\nDivision using assignment operations is: %s"%a

a%=b
print "\nModular division using assignment operations is: %s"%a

a//=b
print "\nFloor division using assignment operations is: %s"%a
#================================================================================
#11	 Bitwise Operator	Read 2 numbers to variable a and b and perform all bitwise operations on that numbers.
#--------------------------------------------------------------------------------
#PYTHON CODE:
a = int(raw_input("Enter a variable"))
b = int(raw_input("Enter b variable"))

print a&b
print a<<b
print a==b
print a!=b
print a>>b
#================================================================================
#12	 Comparison operator	Read 10 numbers from user and find the average of all.
#a) Use comparison operator to check how many numbers are less than average and print them
#b) Check how many numbers are more than average.
#c) How many are equal to average.
#--------------------------------------------------------------------------------
#PYTHON CODE:
s = z = []
i = count = 0
"""
while i < 10:
    a = int(raw_input("Enter %d number" % i))
    s.append(a)
    i += 1
"""
s=[0,1,2,3,4,5,6,7,8,9]
print "Average of list is %f" % (sum(s) / float(len(s)))

#a
for item in s:
    if item < 4.5:
        count+=1
        z.append(item)

print "There are %d numbers whose value is less than average of list."%count
print "They are:"
for item in z:
    print item
#b
count = 0
for item in s:
    if item > 4.5:
        count+=1
print "There are %d numbers whose value is greater than average of list."%count
#c
count = 0
for item in s:
    if item == 4.5:
        count+=1
print "There are %d numbers whose value is equal to average of list."%count

#================================================================================
#13	 Decision making (IF .. Else structure)	Write a program to find the biggest of 4 numbers.
#   a)  Read 4 numbers from user using Input statement.
#   b) extend the above program to find the biggest of 5 numbers.
#( PS : Use IF and IF & Else , If and ELIf, and Nested IF )
#--------------------------------------------------------------------------------
#PYTHON CODE:
s=[]
for item in xrange(0,4):
    inn = input("Enter a number")
    s.append(inn)

s.sort(reverse=True)
print "The biggest number entered is: ",s[0]

inn = input("Enter 5th number")
s.append(inn)
s.sort(reverse=True)
print "The biggest number entered is: ",s[0]

#================================================================================
#14	 Python List functions and Methods	Write a program to create two list A and B such that List A contains Employee Id, List B contains Employee name
#     ( minimum 10 entries in each list ) and perform following operations
#     a) Print all names on to screen
#     b) Read the index from the  user and print the corresponding name from both list.
#     c) Print the names from 4th position to 9th position
#     d) Print all names from 3rd position till end of the list
#     e) Repeat list elements by specified number of times ( N- times, where N is entered by user)
#     f)  Concatenate two lists and print the output.
#     g) Print element of list A and B side by side.( i.e.  List-A First element ,  List-B First element )
#--------------------------------------------------------------------------------
#PYTHON CODE:
empid = [1,2,3,4,5,6,7,8,9,10]
empname = ["a","s","d","f","g","h","i","j","k","l"]

#a
print empname
#b
inn = input("Enter user index")
print "User index points to %d in id and %s in name"%(empid[inn],empname[inn])
#c
print empname[4:10]
#d
print empname[3:]
#e
print empid*inn
print empname*inn
#f
emp = empid+empname
print emp
#g
for item in xrange(0,len(empid)):
    print empid[item]," = ",empname[item]
#================================================================================
#15	 Python List functions and Methods	Create a list of 5 names and check given name exist in the List.
#        a) Use membership operator ( IN ) to check the presence of an element.
#        b) Perform above task without using membership operator.
#        c) Print the elements of the list in reverse direction.
#--------------------------------------------------------------------------------
#PYTHON CODE:
list = ["chris","harry","tony","stark","hulk"]
name = "stark"
#a
if name in list:
    print "%s is present in list"%name
else:
    print "%s is not present in list" % name
#b
for item in list:
    if name == item:
        print "%s is present in list" % name
    else:
        print "%s is not present in list" % name
#c
for item in reversed(list):
    print item
#================================================================================
#16	 Python Numbers	Write program to perform following:
#     i) Check whether given number is prime or not.
#    ii) Generate all the prime numbers between 1 to N where N is given number.
#--------------------------------------------------------------------------------
#PYTHON CODE:
num = input("Enter a number")
#a
def func_prime(digit):
    if digit in [0,1]:
        return 2
    for item in xrange(2, digit):
        if digit % item == 0:
            return 1
            break
        else:
            return 0
            break

res = func_prime(num)
if res == 0:
    print "%d is a prime number"%num
elif res == 1:
    print "%d is not a prime number" % num
elif res == 2:
    print "%d is neither"%num
#b
def func_num(digit):
    count = 0
    i =2
    for item in xrange(2,digit):
        while i <= item:
            #print item,"item"
            if item%i == 0 and item != i:
             #   print item,i
                count += 1
                break
            i+=1
        i=2
        if count == 0:
            print item
        count = 0


func_num(num)
#================================================================================
#17	 Decision making (IF .. Else structure)	Write program to find  the biggest  and Smallest of N numbers.
#      PS: Use the functions to find biggest and  smallest numbers. 
#--------------------------------------------------------------------------------
#PYTHON CODE:list = [1,43,255,34,66,23,52,4,76,5,54,23,6]
print "Biggest of the numbers is %d"%max(list)
print "Smallest of the numbers is %d"%min(list)
#================================================================================
#18	 Looping structures	Using loop structures print numbers from 1 to 100.  and using the same loop print numbers from 100 to 1.( reverse printing)
#     a) By using For loop 
#     b) By using while loop
#    c) Let    mystring ="Hello world"
#             print each character of  mystring in to separate line using appropriate  loop structure.
#--------------------------------------------------------------------------------
#PYTHON CODE:
#a
for item in xrange(1,101):
    print item

#b
i = 1
while i<=100:
    print i
    i+=1
#c
mystring = "Hello world"
for item in mystring:
    print item
#================================================================================
#19	 Looping structures	Using loop structures print even numbers between 1 to 100.  
#     a) By using For loop , use continue/ break/ pass statement to skip  odd numbers.
#           i)  break the loop if the value is 50
#           ii) Use continue for the values 10,20,30,40,50

#     b) By using while loop, use continue/ break/ pass statement to skip odd numbers.
#            i)  break the loop if the value is 90
#           ii) Use continue for the values 60,70,80,90
#--------------------------------------------------------------------------------
#PYTHON CODE:
#a
for item in xrange(1,100):
    if item%2 == 0:
        if item in [10, 20, 30, 40]:
            continue
        print item
    else:
        pass
    if item == 50:
        break

#b
i = 0
while i<100:
    i += 1
    if i%2 == 0:
        if i in [60, 70, 80]:
            continue
        print i
    else:
        pass
    if i == 90:
        break

#================================================================================
#20	 Looping structures	Write a program to generate Fibonacci series of numbers.
#    Starting numbers are 0 and 1,  new number in the series is generated by adding previous two numbers in the series.
#  Example : 0, 1, 1, 2, 3, 5, 8,13,21,.....
#   a) Number of elements printed in the series should be N numbers, Where N is any +ve integer.
#   b) Generate the series until the element in the series is less than Max number.
#--------------------------------------------------------------------------------
#PYTHON CODE:
n = 10
s = []
i = 1
s.append(0)
s.append(1)
#a
while len(s) < n:
    s.append(s[i]+s[i-1])
    i+=1
print s
#b
s = []
i = 1
s.append(0)
s.append(1)
while s[len(s)-1] < max(s):
    s.append(s[i]+s[i-1])
    i+=1
print s
#================================================================================
